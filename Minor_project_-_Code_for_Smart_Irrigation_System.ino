//Libraries
#include <LiquidCrystal.h>

//Sensor and valve pins
#define soil_pin A0
#define valve 8

// lcd pins
const int rs = 2, en = 3, d4 = 4, d5 = 5, d6 = 6, d7 = 7;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Declare data variables
int soil_value;
uint32_t delayMS;
int min_moist_percentage;

// setup function runs once and prepares the system for running the program
void setup() {
  // Initialize serial monitor
  Serial.begin(9600);
  Serial.println("Smart Irrigation System");

  // Splash screen
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Smart Irrigation ");
  lcd.setCursor(0, 1);
  lcd.print("System");
  delay(2000);

  lcd.clear();

  lcd.setCursor(0, 0);
  lcd.print("Waiting for");
  lcd.setCursor(0, 1);
  lcd.print("user input");

  // Setting minimum moisture content
 
  while (Serial.available() == 0) {
     // Delay between measurements.
     delay(1000);

      //Read analog signal from soil moisture sensor
      soil_value = analogRead(soil_pin);
      // convert it to percentage
      soil_value = map(soil_value, 0, 1023, 100, 0);

      // display on serial monitor
      Serial.print("Current moisture reading: ");
      Serial.print(soil_value);
      Serial.println("%");
      Serial.println("Set minimum moisture content");
      Serial.println("");
  }
   
  min_moist_percentage = Serial.parseInt();

  Serial.println("Setting minimum moisture value as ");
  Serial.println(min_moist_percentage);
  Serial.println();
  Serial.println("--------------------------------------------------------------------");
  Serial.println();
  Serial.println("Starting to monitor...");
  Serial.println();
}

// loop is run by Arduino over and over until the reset button
// is pressed
void loop() {

  // Delay between measurements.
  delay(1000);

  //Read analog signal from soil moisture sensor
  soil_value = analogRead(soil_pin);
  // convert it to percentage
  soil_value = map(soil_value, 0, 1023, 100, 0);

  // display on serial monitor
  Serial.print("Moisture Amount : ");
  Serial.print(soil_value);
  Serial.println("%");

  //LCD Display
  lcd.setCursor(0, 0);
  lcd.print("Moisture: ");
  lcd.print(soil_value);
  lcd.println("%");

  //condition for water valve controlling
  lcd.setCursor(0, 1);
  if (soil_value < min_moist_percentage) {
    digitalWrite(valve, HIGH);
    Serial.println("Water supply on");
    lcd.println("Supply on");
  }
  else {
    digitalWrite(valve, LOW);
    Serial.println("Water supply off");
    lcd.println("Supply off");
  }

  Serial.println();
  Serial.println("--------------------------------------------------------------------");
  Serial.println();
}
